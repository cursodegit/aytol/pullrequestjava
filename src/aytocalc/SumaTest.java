package aytocalc;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SumaTest {

	@Test
	void testSumak100() {
		Sumak100 calculadora= new Sumak100();
		double resultado = calculadora.calcula(10.0);
		assertEquals(110.0, resultado);
		}

}
